package facci.pm.rodriguezchoez.selectorfechahora;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;


import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText textFecha,textHora;
    Button btnFecha,btnHora;
    private int dia,mes,anio,hora,minutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textFecha = (EditText)findViewById(R.id.txtFecha);
        textHora = (EditText)findViewById(R.id.txtHora);

        btnFecha = (Button) findViewById(R.id.btnFecha);
        btnHora = (Button) findViewById(R.id.btnHora);
        btnFecha.setOnClickListener(this);
        btnHora.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v==btnFecha){

            final Calendar c= Calendar.getInstance();
            dia = c.get(Calendar.DAY_OF_MONTH);
            mes = c.get(Calendar.MONTH);
            anio = c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    textFecha.setText(i+"/"+(i1+1)+"/"+i2);
                }
            }
                    ,anio,mes,dia);
            datePickerDialog.show();


        }
        if (v==btnHora){
            final Calendar c= Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    textHora.setText(i+":"+i1);
                }
            },hora,minutos,false);
            timePickerDialog.show();


        }

    }

}
